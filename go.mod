module github.com/AOrps/rebxlance

go 1.17

require (
	github.com/lxn/walk v0.0.0-20210112085537-c389da54e794
	github.com/piquette/finance-go v1.0.0
)

require (
	github.com/akavel/rsrc v0.10.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/lxn/win v0.0.0-20210218163916-a377121e959e // indirect
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
)
